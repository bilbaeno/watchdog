/* directory.h */
#ifndef DIRECTORY_H_
#define DIRECTORY_H_

typedef struct Entries Entry;

// allocate head
void allocateEntry(Entry **head);

// add entry to book
void addEntry(Entry *head);

// list all entries in book
void listEntries(Entry *head);

// delete entry from book
void deleteEntry(Entry *head, int id);

#endif /* DIRECTORY_H_ */
