/* directory.c */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct Entries {
    char *firstname;
    char *lastname;
    char *phone;
    struct Entries *next;
} Entry;

/*  allocateEntry(**head)
 *      - malloc *head pointer
 *      - malloc firstname[16], lastname[16], phone[10]
 *      - set head->next to NULL
 */
void allocateEntry(Entry **head) {
    *head = (Entry*) malloc(sizeof(Entry));
    (*head)->firstname  = (char*) malloc(sizeof(char) * 16);
    (*head)->lastname   = (char*) malloc(sizeof(char) * 16);
    (*head)->phone      = (char*) malloc(sizeof(char) * 10);
    (*head)->next = NULL;
}


/*  addEntry(*head);
 *      - allocate new entry
 *      - allocate buffer for user input
 *      - prompt user for input: firstname, lastname, phone
 *      - print newly created entry
 *      - link new entry to end of list *head
 */
void addEntry(Entry *head) {
    // allocate new entry
    Entry *new;
    allocateEntry(&new);
    
    // allocate buffer for user input
    char *buffer = (char*) malloc(sizeof(char) * 128);
    
    // prompt user for input: firstname, lastname, phone
    while (!false) {    
        puts("Enter first name:");
        scanf("%s", buffer);
        if ( strlen(buffer) <= 16 ) {
            strcpy(new->firstname, (const char*) buffer);
            break;
        } else {
            puts("First name cannot exceed 16 characters.");
        }
    }
    while (!false) {
        puts("Enter last name:");
        scanf("%s", buffer);
        if ( strlen(buffer) <= 16 ) {
            strcpy(new->lastname, (const char*) buffer);
            break;
        } else {
            puts("Last name cannot exceed 16 characters.");
        }
    }
    while (!false) {
        puts("Enter phone number:");
        scanf("%s", buffer);
        if ( strlen(buffer) <= 10) {
            strcpy(new->phone, (const char*) buffer);
            break;
        } else {
            puts("Phone number cannot exceed 10 characters.");
        }
    }
    printf("\nNew entry created:\n  First: %s\n  Last: %s\n  Phone: %s\n\n", 
        new->firstname, new->lastname, new->phone);

    // link new entry to end of list *head
    Entry *last = head;
    while (last->next != NULL) { last = last->next; }
    last->next = new;
}

/*  listEntries(*head)
 *      - check if list *head is empty
 *      - initialize ptr for indexing
 *      - traverse linked list and print entries
 */
void listEntries(Entry *head) {
    // check if list *head is empty
    if (head->next == NULL) { printf("\nERROR - No entries to list.\n\n"); return; }
    printf("\nCurrent Directory:\n");

    // initialize ptr for indexing
    Entry *ptr = head->next;

    // traverse list and print entries
    int count = 1;
    while (ptr != NULL) {
        printf("%d:", count);
        printf(" %s %s, %s\n", ptr->firstname, ptr->lastname, ptr->phone);
        ptr = ptr->next;
        count++;
    }
    printf("\n");
}

/*  deleteEntry(*head, id)
 *      - initialize *curr and *next for indexing
 *      - check if list *head is empty
 *      - traverse list for entry number ID and delete
 *      - warn user if id is out of range
 */
void deleteEntry(Entry *head, int id) {
    int count = 1;
    // initialize *curr and *next for indexing
    Entry *curr = head;
    Entry *next = head->next;

    // check if list *head is empty
    if (next == NULL) { printf("\nERROR - The directory is empty.\n\n"); return; }    

    // traverse list for entry number ID and delete
    while (next != NULL) {
        if (count == id) {
            curr->next = next->next;
            break;
        } else {
            curr = next;
            next = next->next;
            count++;
        }
    }
}


