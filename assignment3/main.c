/* main.c */
#include <stdbool.h>
#include <stdio.h>
#include "directory.h"


int main () {
    
    Entry *book;            // initialize book
    allocateEntry(&book);   // allocate head entry
    
    // USER INPUT LOOP
    while (!false) {
        int option; // for switch cases
        int id;     // for deleting entries
        
        printf("Please choose an option:\n");
        printf("1. Instert a new entry.\n");
        printf("2. Delete an entry\n");
        printf("3. Display current directory.\n");
        printf("4. Exit the program.\n");
        puts("Option: ");
        scanf("%d", &option);
        
        // USER INPUT CASES
        switch(option) {
        case 1:
            addEntry(book);
            break;
        case 2:
            puts("Enter the number of the entry you would like to delete:");
            scanf("%d", &id);
            deleteEntry(book, id);
            break;
        case 3:
            listEntries(book);
            break;
        case 4:
            return 0;
        default:
            printf("\nERROR - That is not an option.\n");
        }
    }   
} /* end main() */
