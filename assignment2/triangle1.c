#include <stdio.h>

void print5Triangle() {
    int i, j;
    int h = 5, w = 9;
    char triangle[h][w];

    /* Initialize Triangle */
    for (i = 0; i < h; i++) {
        int guide = (w/2) - i;
        int count = 0;
        for (j = 0; j < w; j++) {
            if (j < guide || j >= (w-guide)) {
                triangle[i][j] = ' ';
            } else {
                triangle[i][j] = (char) (((int)'0') + count);
                count++;
            }
        }
    }

    /* Print Triangle */
    
    for (i = 0; i < h; i++) {
        for (j = 0; j < w; j++) {
            if (triangle[i][j] != '\0') { 
                printf("%c ", triangle[i][j]);        
            }
        }
        printf("\n");
    }
}
