#include <stdlib.h>
#include <stdio.h>

/* Allocate a triangle of height "height" (a 2-D array of int) */
void allocateNumberTriangle(const int height, int ***triangle) {

    int width = (height * 2) - 1;    
    int i;
    *triangle = (int **) malloc (height * sizeof(int*));
    for (i = 0; i < height; i++) {
        (*triangle)[i] = (int *) malloc (width * sizeof(int) );
    }
}

/* Initialize the 2-D triangle array */
void initializeNumberTriangle(const int height, int **triangle) {

    int width = (height * 2) - 1;    
    int i, j;
    for (i = 0; i < height; i++) {
        int guide = (width/2) - i;
        int count = 0;
        for (j = 0; j < width; j++) {
            if (j < guide || j >= (width-guide)) {
                triangle[i][j] = ' ';
            } else {
                triangle[i][j] = (char) (((int)'0') + count);
                count++;
            }
        }
    }
}

/* Print a formatted triangle */
void printNumberTriangle(const int height, int **triangle) {

    int width = (height * 2) - 1;
    int i, j;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            if (triangle[i][j] != '\0') { 
                printf("%c ", triangle[i][j]);        
            }
        }
        printf("\n");
    }
}

/* Free the memory for the 2-D triangle array */
void deallocateNumberTriangle(const int height, int **triangle) {

    int width = (height * 2) - 1;
    int i;
    for (i = 0; i < height; i++) {
        free(triangle[i]);
    }
    free(triangle);
}

