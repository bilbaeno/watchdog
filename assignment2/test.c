#include <stdlib.h>
#include <stdio.h>

#include "triangle.h"

int main() {
    /* Problem 1 - */
    print5Triangle();
    
    /* Problem 2 - */
    int height = 0;
    int **triangle;
    
    while (height < 1 || height > 5) {
        puts("Please enter the height of the triangle [1-5]:");
        scanf("%d",&height);
    }
    
    allocateNumberTriangle  ( (const int) height, &triangle);
    initializeNumberTriangle( (const int) height, triangle);
    printNumberTriangle     ( (const int) height, triangle);
    deallocateNumberTriangle( (const int) height, triangle);

    return 0;
}
