/*
 *	test-caesar.cpp	
 *	based on test-rot13cipher.cpp provided by norris for cis330 
 *
 *		Author: bilbaeno
 */

#include <iostream>
#include <fstream>

#include "ioutils.hpp"
#include "cipher.hpp"
#include "caesar.hpp"

int main(int argc, const char *argv[]) {

	IOUtils io;
	io.openStream(argc,argv);
	std::string input, encrypted, decrypted;
	input = io.readFromStream();
	std::cout << "Original text:" << std::endl << input;

	// 1. Test caesar cipher
	CaesarCipher caesar(30);
	encrypted = caesar.encrypt(input);
	std::cout << "Encrypted text:" << std::endl << encrypted << std::endl;

	decrypted = caesar.decrypt(encrypted);
	std::cout << "Decrypted text:" << std::endl << decrypted << std::endl;

	if (decrypted == input) std::cout << "Decrypted text matches input!" << std::endl;
	else {
		std::cout << "Oops! Decrypted text doesn't match input!" << std::endl;
		return 1;   // Make sure to return a non-zero value to indicate failure
	}

	// 2. Test date cipher
		// unimplemented

	return 0;
}
