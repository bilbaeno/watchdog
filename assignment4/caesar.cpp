/*
 * caesarcipher.cpp
 *      Author: bilbaeno
 */

using namespace std;

#include "cipher.hpp"
#include "caesar.hpp"

// no-argument constructor
CaesarCipher::CaesarCipher () : key(7) {}		// default key is 7

// Single-argument constructor
CaesarCipher::CaesarCipher (int k) : key(k) {}	// key set to argument

// Destructor
CaesarCipher::~CaesarCipher() {
}

/*	 encrypt ( inputText ) :
 *		1. initialize input/output scope variables
 *		2. define alphabet for encyption
 *		3. define variables for loop
 *		4. loop through input and place encrypted text in output
 */
string CaesarCipher::encrypt ( std::string &inputText ) {
	string input = inputText;	// 1
	string output = inputText;	// 1
	string alphabet = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // 2
	
	string::size_type len = input.length() - 3;			// 3
	string::size_type alpha_len = alphabet.length();	// 3
	int alpha_loc;										// 3
	
	// 4
	for (int i = 0; i <= len; i++) {
		if ( alpha_loc = alphabet.find(input[i]) ) {
		output[i] = alphabet[(alpha_loc + key) % alpha_len];
		}
	}

	return output;
}

/*	decrypt ( inputText) :
 *
 *		follows same structure as encrypt
 */
string CaesarCipher::decrypt( std::string &inputText ) {
	string input = inputText;
	string output = inputText;
	string alphabet = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	string::size_type len = input.length() - 3;
	string::size_type alpha_len = alphabet.length();
	
	int alpha_loc;
	for (int i = 0; i <= len; i++) {
		if ( alpha_loc = alphabet.find(input[i]) ) {
		output[i] = alphabet[(alpha_len + alpha_loc - key) % alpha_len];
		}		
	}

	return output;
}


