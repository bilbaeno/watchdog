/*
 * datecipher.hpp
 *      Author: bilbaeno
 */

#ifndef DATECIPHER_HPP_
#define DATECIPHER_HPP_

#include "cipher.hpp"

class DateCipher : public Cipher {
public:
	DateCipher();
	DateCipher(int m, int d, int y);
	virtual ~DateCipher();
	virtual std::string encrypt( std::string &inputText );
	virtual std::string decrypt( std::string &inputText );
private:
	int *date_arr = new int[3];
};

#endif /* DATECIPHER_HPP_ */
