/*
 * datecipher.cpp
 *      Author: bilbaeno
 */

using namespace std;

#include "cipher.hpp"
#include "datecipher.hpp"

// no-argument constructor
DateCipher::DateCipher () {
	date_arr[0] = 3;
	date_arr[1] = 5;
	date_arr[2] = 7;
}

// 3-argument constructor
DateCipher::DateCipher (int m, int d, int y) {
	date_arr[0] = m;
	date_arr[1] = d;
	date_arr[2] = y;
}

// Destructor
DateCipher::~DateCipher() {
}

/*	 encrypt ( inputText ) :
 *		1. initialize input/output scope variables
 *		2. define alphabet for encyption
 *		3. define variables for loop
 *		4. loop through input and place encrypted text in output
 */
string DateCipher::encrypt ( string &inputText ) {
	string input 	= inputText;	// 1
	string output 	= inputText;	// 1
	string alphabet = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ";	// 2
	
	string::size_type len 		= input.length() - 3;	// 3
	string::size_type alpha_len = alphabet.length();	// 3
	int alpha_loc;										// 3
	
	//	4
	for (int i = 0; i <= len; i++) {
		if ( alpha_loc = alphabet.find(input[i]) ) {
			output[i] = alphabet[ (alpha_loc + alpha_len + date_arr[i%3]) % alpha_len ];
		}
	}

	return output;
}


/*	decrypt ( inputText) :
 *
 *		follows same structure as encrypt
 */
string DateCipher::decrypt( string &inputText ) {
	string input 	= inputText;
	string output 	= inputText;
	string alphabet = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	string::size_type len 		= input.length() - 3;
	string::size_type alpha_len = alphabet.length();
	
	int alpha_loc;
	for (int i = 0; i <= len; i++) {
		if ( alpha_loc = alphabet.find(input[i]) ) {
			output[i] = alphabet[(alpha_loc + alpha_len - date_arr[i%3]) % alpha_len];
		}
	}
	
	return output;
}


