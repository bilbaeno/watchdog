/*
 * caesarcipher.hpp
 *      Author: bilbaeno
 */

#ifndef CAESARCIPHER_HPP_
#define CAESARCIPHER_HPP_

#include "cipher.hpp"

class CaesarCipher : public Cipher {
public:
	CaesarCipher();
	CaesarCipher(int key);
	virtual ~CaesarCipher();
	virtual std::string encrypt( std::string &inputText );
	virtual std::string decrypt( std::string &inputText );
private:
	int key;
};

#endif /* CAESARCIPHER_HPP_ */
