/*
 *	test-date.cpp	
 *	based on test-rot13cipher.cpp provided by norris for cis330 
 *
 *		Author: bilbaeno
 */

#include <iostream>
#include <fstream>

#include "ioutils.hpp"
#include "cipher.hpp"
#include "datecipher.hpp"

int main(int argc, const char *argv[]) {

	IOUtils io;
	io.openStream(argc,argv);
	std::string input, encrypted, decrypted;
	input = io.readFromStream();
	std::cout << "Original text:" << std::endl << input;

	// 1. Test date cipher
	DateCipher date(10, 1, 31);
	encrypted = date.encrypt(input);
	std::cout << "Encrypted text:" << std::endl << encrypted << std::endl;

	decrypted = date.decrypt(encrypted);
	std::cout << "Decrypted text:" << std::endl << decrypted << std::endl;

	if (decrypted == input) std::cout << "Decrypted text matches input!" << std::endl;
	else {
		std::cout << "Oops! Decrypted text doesn't match input!" << std::endl;
		return 1;   // Make sure to return a non-zero value to indicate failure
	}

	return 0;
}
