#ifndef MONITOR_HPP_
#define MONITOR_HPP_

#include "alarm.hpp"

class Monitor {
  protected:
	int	count;
	int total;
	Alarm *watchList[];
  public:
	Monitor() 		: count(0), total(10) { watchList[10]; }
	Monitor(int t) 	: count(0), total(t) { watchList[t]; }
	~Monitor() {}

	void addAlarm(Alarm* a);
	void updateAll();
	void statusAll();
	void list();
};

void
Monitor::addAlarm(Alarm* a) {
	if (count <= total) {
		watchList[count] = a;
		count++;
		Serial.print("Monitor::addAlarm -- Alarm successfully added.\n");
	} else { Serial.print("Monitor::addAlarm -- Monitor is full. Alarm not added.\n"); }
}

void
Monitor::updateAll() {
	Serial.print("Monitor::updateAll() --\n"); // TEST PRINT LINE
	for (int i=0; i<count; i++) { watchList[i]->update(); }
}

void
Monitor::statusAll() {
	Serial.print("Monitor::statusAll() -- \n"); // TEST PRINT LINE
	for (int i=0; i<count; i++) { watchList[i]->status(); }
}

void
Monitor::list() {
	Serial.print("Monitor::list() -- \n");
	if (count == 0) { Serial.print(" Monitor contains no alarms.\n"); return; }
	
	Serial.print("	Alarms: ");
	for (int i=0; i<count; i++) {
		Serial.print("\n	");
		Serial.print(i); Serial.print(": "); Serial.print( watchList[i]->title() );
	}
	Serial.print("\n\n\n");
}
#endif /*MONITOR_HPP_*/
