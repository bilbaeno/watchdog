/*
January 28, 2014
[author] Mark Graziano
[email] mark.graziano.13@gmail.com
[instructables profile] http://www.instructables.com/member/GraziCNU/
*/

int LEDArray[] = {13};           // LED array for 
int numOfLEDs = 1;                       // LEDs of index 0-3
int pirState = LOW;
int pirVal = 0;
int pirPin = 13;
int noisePin = 12;

//the time we give the sensor to calibrate (10-60 secs according to the datasheet)
int calibrationTime = 30;

//the time when the sensor outputs a low impulse
long unsigned int lowIn;         

//the amount of milliseconds the sensor has to be low 
//before we assume all motion has stopped
long unsigned int mypause = 5000;  

boolean lockLow = true;
boolean takeLowTime;  


void setup()  {
 
  Serial.begin(9600);
  pinMode(pirPin, INPUT);
  digitalWrite(pirPin, LOW);
  //give the sensor some time to calibrate
  Serial.print("calibrating sensor ");
  for(int i = 0; i < calibrationTime; i++) {
      Serial.print(".");
      delay(1000);
   }
  Serial.println(" done");
  Serial.println("SENSOR ACTIVE");
  delay(50);
  pinMode(noisePin, OUTPUT);
  int i;
  for(i = 0; i < numOfLEDs; i++) {
    pinMode(LEDArray[i], OUTPUT);
    digitalWrite(LEDArray[i], LOW);
  }
}

void loop()  {

  if (digitalRead(pirPin) == HIGH) {
    // Turn lights on and make some noise
    for (int i= 0; i < numOfLEDs; i++) digitalWrite(LEDArray[i], HIGH);
    for (int i= 0; i < numOfLEDs; i++) digitalWrite(LEDArray[i], LOW);
    digitalWrite(5,LOW);
    for (int j = 0; j< 5; j++) { 
      digitalWrite(noisePin, HIGH); 
    }
    if(lockLow){ 
      
      //makes sure we wait for a transition to LOW before any further output is made:
      lockLow = false;            
      Serial.println("---");
      Serial.print("motion detected at ");
      Serial.print(millis()/1000);
      Serial.println(" sec"); 
      delay(50);
     }         
     takeLowTime = true;
  }

  digitalWrite(5,LOW);
  for (int i= 0; i < numOfLEDs; i++) digitalWrite(LEDArray[i], LOW);

  if(digitalRead(pirPin) == LOW) {       
    // Turn lights off and stop making noise
    digitalWrite(noisePin, LOW);
    for (int i= 0; i < numOfLEDs; i++) digitalWrite(LEDArray[i], LOW);

    if(takeLowTime){
      lowIn = millis();          //save the time of the transition from high to LOW
      takeLowTime = false;       //make sure this is only done at the start of a LOW phase
    }
    //if the sensor is low for more than the given pause, 
    //we assume that no more motion is going to happen
    if (!lockLow && millis() - lowIn > mypause) {  
       //makes sure this block of code is only executed again after 
       //a new motion sequence has been detected
       lockLow = true;                        
       Serial.print("motion ended at ");      //output
       Serial.print((millis() - mypause)/1000);
       Serial.println(" sec");
       delay(50);
    }
 
  }
	Serial.println( analogRead(12) );
}



