/*
 HC-SR04 Ping distance sensor]
 VCC to arduino 5v GND to arduino GND
 Echo to Arduino pin 13 Trig to Arduino pin 12
 Red POS to Arduino pin 11
 Green POS to Arduino pin 10
 560 ohm resistor to both LED NEG and GRD power rail
 More info at: http://goo.gl/kJ8Gl
 Original code improvements to the Ping sketch sourced from Trollmaker.comw
 Some code and wiring inspired by http://en.wikiversity.org/wiki/User:Dstaub/robotcar

		modified by 'bilbaeno'
 */

#define trigPin 9
#define echoPin 11
#define led 13
#define led2 13

void setup() {
	Serial.begin (9600);
	pinMode(trigPin, OUTPUT);
	pinMode(echoPin, INPUT);
	pinMode(led, OUTPUT);
	pinMode(led2, OUTPUT);
}

void loop() {
	long distance;
	
	digitalWrite(trigPin, LOW);
	delayMicroseconds(2);
	digitalWrite(trigPin, HIGH);
	delayMicroseconds(10);
	digitalWrite(trigPin, LOW);
	
//	duration = pulseIn(echoPin, HIGH);
	distance = ( pulseIn(echoPin, HIGH) /2) / 29.1;
	
  	if (distance < 15) {
		digitalWrite(led,HIGH);
	} else {
    	digitalWrite(led,LOW);
	}
  
	if (distance >= 200 || distance <= 0) {
		Serial.println("Out of range");
	} else {
		Serial.print(distance);
		Serial.println(" cm");
	}
	delay(200);
}
