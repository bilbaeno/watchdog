/*
 * cloudalarm.hpp
 *      Author: bilbaeno
 */

#ifndef CLOUDALARM_HPP_
#define CLOUDALARM_HPP_

#include <SPI.h>
#include <PubSubClient.h>
#include <Ethernet.h>
#include "alarm.hpp"
#include "mqttclient.hpp"


class CloudAlarm: public Alarm {
public:
	CloudAlarm();
	~CloudAlarm();
	
	Tripwire tripwire;
	MQTTClient mqtt;
	
	void trigger();
};

#endif /* CLOUDALARM_HPP_ */
