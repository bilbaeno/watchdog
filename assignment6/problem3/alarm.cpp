/*
 * alarm.cpp
 *      Author: bilbaeno
 */

#include "alarm.hpp"

Alarm::Alarm() {
	buzzerPin = A3;
	ledPin = 13;
	pinMode( buzzerPin, OUTPUT);
	pinMode( ledPin, OUTPUT);
}

Alarm::~Alarm() {
}

void
Alarm::triggerBuzzer() {
	analogWrite( buzzerPin, 500 );
	delay( 500 );
	analogWrite( buzzerPin, 0 );
}

void
Alarm::triggerLED() {
	analogWrite( ledPin, HIGH );
	delay( 500 );
	analogWrite( ledPin, LOW );
}

bool
Alarm::engaged() {
	if ( tripwire.read() < tripwire.getLevel() ) {
		return true;
	} else {
		return false;
	}
}
