/*
 * tripwire.hpp
 *      Author: bilbaeno
 */

#ifndef TRIPWIRE_HPP_
#define TRIPWIRE_HPP_

#include <Arduino.h>

class Tripwire {
public:
	Tripwire();
	Tripwire(int l);
	~Tripwire();

	int read()		{ return analogRead( photoPin ); }
	int getLevel()	{ return photoLevel; }
	
protected:
	int photoPin;
	int photoLevel;
};

#endif /* TRIPWIRE_HPP_ */
