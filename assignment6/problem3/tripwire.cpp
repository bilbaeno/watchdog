/*
 * tripwire.cpp
 *      Author: bilbaeno
 */

#include "tripwire.hpp"

Tripwire::Tripwire() {
	photoPin = A5;
	photoLevel = 400;
}

Tripwire::Tripwire(int l) {
	photoLevel = l;
}

Tripwire::~Tripwire() {
}
