/*
 * alarm.hpp
 *      Author: bilbaeno
 */

#ifndef ALARM_HPP_
#define ALARM_HPP_

#include "tripwire.hpp"

class Alarm {
public:
	Alarm();
	~Alarm();
	
	Tripwire tripwire;
	
	void triggerBuzzer();
	void triggerLED();
	bool engaged();
	int getBuzzerPin() { return buzzerPin; }
	int getLEDPin() { return ledPin; }

protected:
	int buzzerPin;
	int ledPin;
};

#endif /* ALARM_HPP_ */
