/*
 * cloudalarm.cpp
 *      Author: bilbaeno
 */

#include "cloudalarm.hpp"

CloudAlarm::CloudAlarm() {
	mqtt.begin("localhost", 1933, OPEN, NULL, NULL, NULL);
	//mqtt.begin("brix.d.cs.uoregon.edu", 8330, OPEN, NULL, NULL, NULL);
}

CloudAlarm::~CloudAlarm() {
}

void
CloudAlarm::trigger() {
	mqtt.publish("edison/laseralarm", "laser tripped!");
	//mqtt.publish("cis330/bilbaeno", "laser tripwire on bilbaeno's edison has been tripped!");
}

