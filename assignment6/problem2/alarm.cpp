/*
 * alarm.cpp
 *      Author: bilbaeno
 */

#include "alarm.hpp"

Alarm::Alarm() {
}

Alarm::~Alarm() {
}

void
Alarm::triggerBuzzer() {
	analogWrite( buzzerPin, 50 );
	delay( 500 );
	analogWrite( buzzerPin, 0 );
}

void
Alarm::triggerLED() {
	analogWrite( ledPin, 50 );
	delay( 500 );
	analogWrite( ledPin, 0 );
}

bool
Alarm::engaged() {
	if ( tripwire.read() > tripwire.getLevel() ) {
		return true;
	} else {
		return false;
	}
}
