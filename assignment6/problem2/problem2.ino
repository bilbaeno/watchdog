/*
 * problem3.ino
 *      Author: bilbaeno
 *
 *	NOTE TO GRADERS:
 *		This currently does the opposite of a tripwire. Found 
 */


#include "alarm.hpp"

Alarm alarm;

void setup() {
	pinMode( alarm.getBuzzerPin(), OUTPUT );
	pinMode( alarm.getLEDPin(), OUTPUT );
	Serial.begin(9600);
}

void loop() {
	if ( alarm.engaged() == false ) {
		alarm.triggerLED();
	}
	delay(50);
}
