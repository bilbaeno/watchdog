/* encoder.hpp */
#ifndef ENCODER_HPP_
#define ENCODER_HPP_

#include "blinker.hpp"

void encode(char &c, Blinker &b) {
	char ch = c;
	switch(c)
	{
		case 'a':  b.dot(); 	b.dash();	break;
		case 'b':  b.dash(); 	b.dot();	b.dot(); 	b.dot();	break;
		case 'c':  b.dash(); 	b.dot();	b.dash();	b.dot();	break;
		case 'd':  b.dash(); 	b.dot();	b.dot();	break;
		case 'e':  b.dot();		break;
		case 'f':  b.dot(); 	b.dot();	b.dash();	b.dot();	break;
		case 'g':  b.dash(); 	b.dash();	b.dot();	break;
		case 'h':  b.dot(); 	b.dot();	b.dot();	b.dot();	break;
		case 'i':  b.dot(); 	b.dot();	break;
		case 'j':  b.dot(); 	b.dash();	b.dash();	b.dash();	break;
		case 'k':  b.dash(); 	b.dot();	b.dash();	break;
		case 'l':  b.dot();		b.dash();	b.dot();	b.dot();	break;
		case 'm':  b.dash(); 	b.dash();	break;
		case 'n':  b.dash();	b.dot();	break;
		case 'o':  b.dash();	b.dash();	b.dash();	break;
		case 'p':  b.dot();		b.dash();	b.dash();	b.dot();	break;
		case 'q':  b.dash();	b.dash();	b.dot();	b.dash();	break;
		case 'r':  b.dot();		b.dash();	b.dot();	break;
		case 's':  b.dot();		b.dot();	b.dot();	break;
		case 't':  b.dash();	break;
		case 'u':  b.dot();		b.dot();	b.dash();	break;
		case 'v':  b.dot();		b.dot();	b.dot();	b.dash();	break;
		case 'w':  b.dot();		b.dash();	b.dash();	break;
		case 'x':  b.dash();	b.dot();	b.dot();	b.dash();	break;
		case 'y':  b.dash();	b.dot();	b.dash();	b.dash();	break;
		case 'z':  b.dash();	b.dash();	b.dot();	b.dot();	break;
		default:   break;
	}
	b.space();
}

#endif /* ENCODER_HPP_ */
