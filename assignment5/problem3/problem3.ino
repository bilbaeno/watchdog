/*
  
 */

#include "blinker.hpp"
#include "encoder.hpp"

int pin = 13;
int len = 200;
Blinker blinker(pin, len);

char testchar [4] = {'a', 'd', 'a', 'm'};

// the setup function runs once when you press reset or power the board
void setup() {
	// initialize digital pin 13 as an output.
	pinMode(pin, OUTPUT);
	
	blinker.dot();
	blinker.dot();
	blinker.wordspace();
}

// the loop function runs over and over again forever
void loop() {
	for (int i=0; i<4; i++)
		encode(testchar[i], blinker);
	blinker.wordspace();
}
