/* blinker.h */
#ifndef BLINKER_HPP_
#define BLINKER_HPP_

class Blinker {
public:
	Blinker();
	Blinker(int p, int d);
	void dot();
	void dash();
	void space();
	void wordspace();
private:
	int pin;
	int dot_len;
};

#endif /* BLINKER_HPP_ */
