/*
 * blinker.cpp
 *      Author: bilbaeno
 */

#include <Arduino.h>
#include "blinker.hpp"

Blinker::Blinker() {
	pin = 13;
	dot_len = 500;
}

Blinker::Blinker(int p, int d) {
	pin = p;
	dot_len = d;
}

void Blinker::dot() 	{ 
	digitalWrite(pin, HIGH);
	delay(dot_len);
	digitalWrite(pin, LOW);
	delay(dot_len);
}

void Blinker::dash() {
	digitalWrite(pin, HIGH);
	delay(dot_len * 3);
	digitalWrite(pin, LOW);
	delay(dot_len);
}

void Blinker::space() {
	digitalWrite(pin, LOW);
	delay(dot_len * 2);
}

void Blinker::wordspace() {
	digitalWrite(pin, LOW);
	delay(dot_len * 6);
}

