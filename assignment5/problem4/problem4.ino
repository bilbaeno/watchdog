/*
  
 */

#include <Arduino.h>

int pin_out = 13;
int len = 300;

int current_val;

void setup() {
	pinMode(pin_out, OUTPUT);
	pinMode(A5, INPUT);
	
	digitalWrite(pin_out, HIGH);
	delay(len);
	digitalWrite(pin_out, LOW);
	delay(len);
	digitalWrite(pin_out, HIGH);
	delay(len);
	digitalWrite(pin_out, LOW);
	delay(len * 4);
}

// the loop function runs over and over again forever
void loop() {
	current_val = analogRead(A5);
	if (current_val > 600) {
		digitalWrite(pin_out, HIGH);
		delay(50);
	} else {
		digitalWrite(pin_out, LOW);
	}
}
