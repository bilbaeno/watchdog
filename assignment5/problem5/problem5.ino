/*
  
 */

#include "blinker.hpp"
#include "MorseEncoder.hpp"

int pin = 13;
int len = 300;
Blinker blinker(pin, len);
MorseEncoder encoder(blinker);

char testchar [4] = {'a', 'd', 'a', 'm'};

void setup() {
	pinMode(pin, OUTPUT);
	
	blinker.dot();
	blinker.dot();
	blinker.wordspace();
}

// the loop function runs over and over again forever
void loop() {
	char morse_text[6];
	char input;
	for (int i=0; i<4; i++) {
		input = testchar[i];
		encoder.encodeLED(input);
		encoder.encodeText(input, morse_text);
	}
	blinker.wordspace();
}
