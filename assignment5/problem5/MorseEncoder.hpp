/* MorseEncoder.hpp */
#ifndef MORSEENCODER_HPP_
#define MORSEENCODER_HPP_

#include "blinker.hpp"

class MorseEncoder {
public:
	MorseEncoder();
	MorseEncoder(Blinker &b);
	~MorseEncoder();
	void encodeLED(char &c);
	void encodeText(char &c, char out[]);
private:
	Blinker b;	
};

#endif /* MORSEENCODER_HPP_ */
