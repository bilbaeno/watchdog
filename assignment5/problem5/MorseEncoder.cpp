/* MorseEncoder.cpp */

#include "MorseEncoder.hpp"

MorseEncoder::MorseEncoder() {
	Blinker b();
}

MorseEncoder::MorseEncoder(Blinker &blinker) {
	Blinker b = blinker;
}

MorseEncoder::~MorseEncoder() {
}

void
MorseEncoder::encodeLED(char &c) {
	char ch = c;
	switch(c)
	{
		case 'a':  b.dot(); 	b.dash();	break;
		case 'b':  b.dash(); 	b.dot();	b.dot(); 	b.dot();	break;
		case 'c':  b.dash(); 	b.dot();	b.dash();	b.dot();	break;
		case 'd':  b.dash(); 	b.dot();	b.dot();	break;
		case 'e':  b.dot();		break;
		case 'f':  b.dot(); 	b.dot();	b.dash();	b.dot();	break;
		case 'g':  b.dash(); 	b.dash();	b.dot();	break;
		case 'h':  b.dot(); 	b.dot();	b.dot();	b.dot();	break;
		case 'i':  b.dot(); 	b.dot();	break;
		case 'j':  b.dot(); 	b.dash();	b.dash();	b.dash();	break;
		case 'k':  b.dash(); 	b.dot();	b.dash();	break;
		case 'l':  b.dot();		b.dash();	b.dot();	b.dot();	break;
		case 'm':  b.dash(); 	b.dash();	break;
		case 'n':  b.dash();	b.dot();	break;
		case 'o':  b.dash();	b.dash();	b.dash();	break;
		case 'p':  b.dot();		b.dash();	b.dash();	b.dot();	break;
		case 'q':  b.dash();	b.dash();	b.dot();	b.dash();	break;
		case 'r':  b.dot();		b.dash();	b.dot();	break;
		case 's':  b.dot();		b.dot();	b.dot();	break;
		case 't':  b.dash();	break;
		case 'u':  b.dot();		b.dot();	b.dash();	break;
		case 'v':  b.dot();		b.dot();	b.dot();	b.dash();	break;
		case 'w':  b.dot();		b.dash();	b.dash();	break;
		case 'x':  b.dash();	b.dot();	b.dot();	b.dash();	break;
		case 'y':  b.dash();	b.dot();	b.dash();	b.dash();	break;
		case 'z':  b.dash();	b.dash();	b.dot();	b.dot();	break;
		default:   break;
	}
	b.space();
}

void
MorseEncoder::encodeText(char &c, char out[]) {
	char ch = c;
	char morse [6];
	switch(c)
	{
		case 'a':	morse[0] = '.'; morse[1] = '_'; break;
		case 'b':	morse[0] = '_'; morse[1] = '.'; morse[2] = '.'; morse[3] = '.'; break;
		case 'c':	morse[0] = '_'; morse[1] = '.'; morse[2] = '_'; morse[3] = '.'; break;
		case 'd':	morse[0] = '_'; morse[1] = '.'; morse[2] = '.'; break;
		case 'e':	morse[0] = '.'; break;
		case 'f':	morse[0] = '.'; morse[1] = '.'; morse[2] = '_'; morse[3] = '.'; break;
		case 'g':	morse[0] = '_'; morse[1] = '_'; morse[2] = '.'; break;
		case 'h':	morse[0] = '.'; morse[1] = '.'; morse[2] = '.'; morse[3] = '.'; break;
		case 'i':	morse[0] = '.'; morse[1] = '.'; break;
		case 'j':	morse[0] = '.'; morse[1] = '_'; morse[2] = '_'; morse[3] = '_'; break;
		case 'k':	morse[0] = '_'; morse[1] = '.'; morse[2] = '_'; break;
		case 'l':	morse[0] = '.'; morse[1] = '_'; morse[2] = '.'; morse[3] = '.'; break;
		case 'm':	morse[0] = '_'; morse[1] = '_'; break;
		case 'n':	morse[0] = '_'; morse[1] = '.'; break;
		case 'o':	morse[0] = '_'; morse[1] = '_'; morse[2] = '_'; break;
		case 'p':	morse[0] = '.'; morse[1] = '_'; morse[2] = '_'; morse[3] = '.'; break;
		case 'q':	morse[0] = '_'; morse[1] = '_'; morse[2] = '.'; morse[3] = '_'; break;
		case 'r':	morse[0] = '.'; morse[1] = '_'; morse[2] = '.'; break;
		case 's':	morse[0] = '.'; morse[1] = '.'; morse[2] = '.'; break;
		case 't':	morse[0] = '_'; break;
		case 'u':	morse[0] = '.'; morse[1] = '.'; morse[2] = '_'; break;
		case 'v':	morse[0] = '.'; morse[1] = '.'; morse[2] = '.'; morse[3] = '_'; break;
		case 'w':	morse[0] = '.'; morse[1] = '_'; morse[2] = '_'; break;
		case 'x':	morse[0] = '_'; morse[1] = '.'; morse[2] = '.'; morse[3] = '_'; break;
		case 'y':	morse[0] = '_'; morse[1] = '.'; morse[2] = '_'; morse[3] = '_'; break;
		case 'z':	morse[0] = '_'; morse[1] = '_'; break;
		default:   break;
	}
	out = morse;
}
