/* File: rps.c */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>      
#include <string.h>

char* getUserChoice() {
    /* Prompt the user "Enter rock, paper, or scissors: " and return
       the string they enter */
    char *choice;
    puts("Enter rock (r), paper (p), or scieccors (s): ");
    scanf("%c", choice);
    return choice;
}

char* getComputerChoice() {
    srand (time(NULL));
    /* get a pseudo-random integer between 0 and 2 (inclusive) */
    int randChoice = rand() % 3;

    /* If randChoice is 0, return "rock"; if randChoice is 1, 
     return "paper", and if randChoice is 2, return "scissors". */
    if (randChoice = 0) {
        puts("Computer Choice: Rock\n");
        return "r";
    } else if (randChoice = 1) {
        puts("Computer Choice: Paper\n");
        return "p";
    } else {
        puts("Computer Choice: Scissors\n");
        return "s";
    }
}

char* compare(char* choice1, char* choice2) 
{
    /* Implement the logic of the game here. If choice1 and choice2
     are equal, the result should be "This game is a tie."

     Make sure to use strcmp for string comparison.
     */
    puts("made it here\n");
    return;
}

int main(int argc, char** argv) 
{
    char *userChoice = NULL, *computerChoice = NULL, *outcome = NULL;

    userChoice = getUserChoice();
    computerChoice = getComputerChoice();

    outcome = compare(userChoice, computerChoice);

    printf("You picked %s.\n", userChoice);
    printf("Computer picked %s.\n", computerChoice);
    printf("%s\n", outcome);

    return 0;
}
